#!/usr/bin/env python3

from pprint import pprint
from typing import List
import os
import functools
import itertools
import datetime

import dotenv
import requests


def get_jira_activity() -> List[dict]:
    print((os.environ["JIRA_EMAIL"], os.environ["JIRA_API_KEY"]))
    response = requests.get(
        "https://getpanda.atlassian.net/rest/api/latest/users",
        auth=(os.environ["JIRA_EMAIL"], os.environ["JIRA_API_KEY"]),

    )
    #response.raise_for_status()

    print(response.text)
    response.raise_for_status()

    return []

@functools.lru_cache(maxsize=None)
def project_id_to_project_name(project_id: int) -> str:
    response = requests.get(
        f"https://gitlab.com/api/v4/projects/{project_id}",
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()["path"]


# Filthy alias so it doesn't bloat fstrings.
pid = project_id_to_project_name


def gitlab_start_date(today: datetime.date) -> datetime.date:
    iso_monday = 1
    is_monday = today.isoweekday() == iso_monday

    if is_monday:
        return today - datetime.timedelta(days=4)
    else:
        return today - datetime.timedelta(days=2)


def get_gitlab_activity() -> List[dict]:

    start_time = gitlab_start_date(datetime.date.today())

    response = requests.get(
        "https://gitlab.com/api/v4/events",
        params={
            "after": start_time,
            "per_page": "100",
            # "scope": "all",
        },
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()


def gitlab_keyfunc(activity):
    return (activity.get("action_name", ""), activity["id"])


def display_activity(activities: List[dict]):

    sorted_activities = sorted(activities, key=gitlab_keyfunc)
    grouped_activities = itertools.groupby(sorted_activities, key=gitlab_keyfunc)

    for _activity_type, activities in grouped_activities:
        for activity in activities:
            activity_name = activity.get("action_name")
            if activity_name == "approved":
                print(f"approved:        {activity['target_title']}")
            elif activity_name == "pushed to":
                print(f"pushed to:       {pid(activity['project_id'])}, {activity['push_data']['ref']}, {activity['push_data']['commit_title']}")
            elif activity_name == "pushed new":
                print(f"pushed new:      {pid(activity['project_id'])}, {activity['push_data']['ref']}, {activity['push_data']['commit_title']}")
            elif activity_name == "commented on":
                print(f"commented on:    {activity['target_title']}")
            elif activity_name == "opened":
                print(f"opened:          {activity['target_title']}")
            elif activity_name == "accepted":
                print(f"accepted:        {activity['target_title']}")
            elif activity_name == "created":
                print(f"created project: {pid(activity['project_id'])}")
            elif activity_name == "deleted":
                print(f"deleted:         {pid(activity['project_id'])}, {activity['push_data']['ref']}, {activity['push_data']['commit_title']}")
            else:
                print(f"unknown:         {activity}")


def main():
    dotenv.load_dotenv()

    # jira_activity = get_jira_activity()
    gitlab_activity = get_gitlab_activity()

    # display_activity(jira_activity + gitlab_activity)
    display_activity(gitlab_activity)



if __name__ == "__main__":
    main()
