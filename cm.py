#!/usr/bin/env python3


from pprint import pprint
from typing import List
import os
import functools
import itertools
import datetime
import urllib.parse
import json


import dotenv
import requests


PROJECTS_CONFIG = {
    "backend": {
        "name": "Backend",
        "project_path": "panda-ai1/backend",
        "environments": {
            "production_portal_api": "portal-api",
            "production_admin_api": "admin-api",
            "production_pipeline": "content-pipeline",
            "production_email_sorter": "email-sorter",
            "production_notification_emails": "notification-emails",
            "production_notifications": "notifications",
        }
    },
    "panda-portal": {
        "name": "Panda Portal",
        "project_path": "panda-ai1/panda-portal",
        "environments": {
            "production": "panda-portal",
        },
    },
    "panda-admin": {
        "name": "Panda Admin",
        "project_path": "panda-ai1/panda-admin",
        "environments": {
            "production": "panda-admin",
        },
    },
    "panda-grand": {
        "name": "Panda GRAND",
        "project_path": "panda-ai1/panda-grand",
        "environments": {
            "prod_panda_grand": "panda-grand",
        },
    },
}


@functools.lru_cache(maxsize=None)
def project_id_to_project_name(project_id: int) -> str:
    response = requests.get(
        f"https://gitlab.com/api/v4/projects/{project_id}",
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()["path"]


def get_gitlab_environments(project_path):
    project = urllib.parse.quote_plus(project_path)
    response = requests.get(
        f"https://gitlab.com/api/v4/projects/{project}/environments",
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()


def get_latest_gitlab_deployment(project_path, environment):
    project = urllib.parse.quote_plus(project_path)

    response = requests.get(
        f"https://gitlab.com/api/v4/projects/{project}/deployments",
        params={
            "environment": environment,
            "status": "success",
            "sort" : "desc",
            "per_page": "1",
        },
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()[0]


UNSPECIFIED=object()


def get_gitlab_merge_requests(project_path, updated_after, label=UNSPECIFIED):
    project = urllib.parse.quote_plus(project_path)

    label_param = {} if label is UNSPECIFIED else {"labels": label}

    response = requests.get(
        f"https://gitlab.com/api/v4/projects/{project}/merge_requests",
        params={
            "state": "merged",
            "updated_after": updated_after,
            **label_param,
        },
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()


def get_gitlab_environment(project_path, environment_id):
    project = urllib.parse.quote_plus(project_path)
    response = requests.get(
        f"https://gitlab.com/api/v4/projects/{project}/environments/{environment_id}",
        headers={
            "PRIVATE-TOKEN": os.environ["GITLAB_API_KEY"],
        },
    )
    response.raise_for_status()
    return response.json()



def main():
    print("# CM")
    print()

    print("## Approval")
    print("Sign off by a production release approver")
    print()

    print("## Changes to deploy")
    for project, project_config in PROJECTS_CONFIG.items():
        print("###", project_config["name"])
        # look up project environment
        #environments = get_gitlab_environments(project_config["project_path"])
        deployments = [
            (get_latest_gitlab_deployment(project_config["project_path"], environment_name)["updated_at"], label)
            for environment_name, label in project_config["environments"].items()
        ]

        # If this project is using labels to deal with more than one deployable environment, group by label.
        if len(project_config["environments"].keys()) > 1:
            for deployment, label in deployments:
                merge_requests = get_gitlab_merge_requests(project_config["project_path"], deployment, label)
                if label and merge_requests:
                    print("####", label)
                for merge_request in merge_requests:
                    #print(json.dumps(merge_request, indent=2))
                    print(f"* [{merge_request['title']}]({merge_request['web_url']}) ({merge_request['merged_by']['username']})")
                print()


        oldest_deployment = list(sorted(deployments))[0][0]

        # The string label "None" is used to find Merge Reqeusts with no labels, yes it's silly.
        merge_requests = get_gitlab_merge_requests(project_config["project_path"], oldest_deployment, "None")
        if merge_requests and len(project_config["environments"].keys()) > 1:
            print("#### Unlabeled")
        for merge_request in merge_requests:
            print(f"* [{merge_request['title']}]({merge_request['web_url']}) ({merge_request['merged_by']['username']})")
        print()

    print("## Testing results")
    print("*")
    print()

    print("## Other setup or migration steps")
    print("*")
    print()

    print("## Planned release time")
    print(datetime.datetime.now().strftime("%d-%m-%y %H:%M:%S"), "US/Pacific")
    print()

    print("## Remediation Process")
    print("Rollback to previous version")
    print()

if __name__ == "__main__":
    dotenv.load_dotenv()
    main()

    # Run
    # pandoc cm.md -f markdown -t docx -s -o cm.docx
